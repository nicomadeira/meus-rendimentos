/* eslint-disable no-unused-expressions */
const AccessApplication = require('../page-objects/AccessApplication')
const {
  CUSTOM_TOOLTIP_GRAPH, INFO_GRAPH, MAIN_TITLE, SELECT_FILTER, TEXT_INFO_FILTER,
} = require('../../src/utils/test-ids')
const { HELPER_INFO_GRAPH, PERIOD_FILTER_DATA, TITLE_HEADER } = require('../../src/components/constants-components')

describe('Home', () => {
  beforeEach(() => {
    const accessApp = new AccessApplication()
    accessApp.go()
  })

  it('Display content when application loads', () => {
    cy.get(`[data-testid=${MAIN_TITLE}]`).should('contain', TITLE_HEADER)
    cy.get(`[data-testid=${INFO_GRAPH}]`).should('contain', HELPER_INFO_GRAPH)
  })

  it('Display the graph if request is successful', () => {
    cy.request('GET', Cypress.env('apiURL')).as('apiCall')
    cy.get('@apiCall').should(response => {
      expect(response).to.have.property('status', 200)
    })
    cy.get('.recharts-area-area').should('exist')
  })

  it('Interact with filter must change the page content', () => {
    const optionName = PERIOD_FILTER_DATA[1].name
    cy.get(`[data-testid=${SELECT_FILTER}]`).select(optionName)
    cy.get(`[data-testid=${TEXT_INFO_FILTER}] strong`).should('contain', `${optionName.toLowerCase()}`)
  })

  it('Display tooltip when hover mouse in graph', () => {
    cy.request('GET', Cypress.env('apiURL')).as('apiCall')
    cy.get('@apiCall').should(response => {
      expect(response).to.have.property('status', 200)
    })
    cy.get('.recharts-area').trigger('mousemove', { clientX: 200, clientY: 300 })
    cy.get(`[data-testid=${CUSTOM_TOOLTIP_GRAPH}]`).should('exist')
  })

  it('Persist data in localstorage when period is selected', () => {
    const optionName = PERIOD_FILTER_DATA[1].name
    cy.get(`[data-testid=${SELECT_FILTER}]`).select(optionName)
    cy.window().then(window => {
      const dataInLocalstorage = window.localStorage.getItem(Cypress.env('localStorageKey'))
      expect(dataInLocalstorage).to.exist
      expect(JSON.parse(dataInLocalstorage)).not.to.be.empty
    })
  })
})
