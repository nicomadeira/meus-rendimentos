# Meus rendimentos

Para ver a aplicação em produção, basta acessar o link: [Meus rendimentos](https://meus-rendimentos.netlify.app/)

## Setup

Para este projeto utilizaremos as seguintes ferramentas:

1. [React.js](https://reactjs.org/)
2. [CRA](https://create-react-app.dev/)
3. [Recharts](https://recharts.org/en-US/)
4. [Styled Components](https://www.styled-components.com/docs/basics)
5. [Jest](https://jestjs.io/)
6. [React Testing Library](https://testing-library.com/docs/react-testing-library/intro)
7. [Cypress](https://www.cypress.io/)
8. [Eslint](https://eslint.org/)
9. [Bitbucket Pipelines](https://bitbucket.org/product/br/features/pipelines)
10. [Netlify](https://www.netlify.com/)

---

## Estrutura

A estrutura usará a nova [Context API](https://reactjs.org/docs/context.html) com os [React Hooks](https://reactjs.org/docs/hooks-intro.html):

A arquitetura de pastas está dividida em **pages** para as páginas da aplicação - por exemplo, o componente Home que está desta estrutura é a Homepage do nosso webapp. 

Também teremos uma ramificação para **components**, onde ficarão os componentes que são unitários, como o gráfico que vemos na aplicação. 

O **context** é onde montamos nossa estrutura de dados globais - que poderiam ser local também dependendo da funcionalidade -. 

Já na pasta de **services** são todos os serviços externos que temos, como a requisição para a API ou o uso do [LocalStorage](https://developer.mozilla.org/pt-BR/docs/Web/API/Window/Window.localStorage). 

Foi separada a pasta de **__testes__** para que possamos rodar individualmente todo os tipos de teste, como unitários, integração ou de UI e testes end-to-end automatizados. Quanto ao deploy a aplicação foi sincronizada com a Netlify para que a mesma seja servida sempre que houver um merge na branch master.

---

## Primeira vez rodando o projeto?

### Passo a passo para rodar:

1. Dar um git clone no repositório:
``git clone https://nicomadeira@bitbucket.org/nicomadeira/meus-rendimentos.git``

2. Rodar o comando dentro da pasta da aplicação para instalar os pacotes NPM:
``npm install``

3. Rodar o comando para dar ínicio a aplicação e ver ela rodando no browser:
``npm start``

*Obs: o projeto roda localmente na porta http://localhost:3212*

---

## Desenvolvi uma feature e quero me certificar que não quebrou nada

Com os testes isso fica bem mais fácil, além do pipeline do bitbucket ajudar a não mergear nada com build falho (ou seja, quebrou algum teste, eslint com erros e etc). Para evitar o intermédio de ter que dar um push para certificar de que tudo está correto com a feature, você pode usar um script da aplicação que roda os mesmos comandos que o pipeline, é só digitar o seguinte no terminal:

``./src/scripts/run-before-push.sh``

Caso ocorra erro de permissão é só rodar:

``chmod +x ./src/scripts/run-before-push.sh``

E tentar novamente o primeiro passo.

---

## Testes

Criamos um ambiente com testes unitários, testes de UI e testes automatizados. Para testes de unidade utilizamos o [Jest](https://jestjs.io/), para testes de UI utilizamos a [React Testing Library](https://testing-library.com/docs/react-testing-library/intro) e para testes automatizados utilizamos o [Cypress](https://www.cypress.io/). Os testes unitários e de UI ficam dentro da estrutura: `./src/__tests__`. Já os testes automatizados ficam dentro da pasta: `./cypress`. Temos comandos para rodar cada um individualmente:

**Testes unitários:**

``npm run test:unit``

*Caso queira ver a cobertura basta adicionar a flag dentro da propriedade scripts > test:unit --coverage no package.json*

**Testes de UI:**

``npm run test:ui``

*Caso queira ver a cobertura basta adicionar a flag dentro da propriedade scripts > test:ui --coverage no package.json*

**Testes automatizados:**

* Para rodar no browser:

  ``npm run cypress:open``

* Para rodar direto no terminal:

  ``npm run test:e2e``

---

## Build

Caso queira rodar o build na mão, é só rodar o comando `npm run build` que gerará uma pasta `./build`. Tudo que está dentro dessa pasta é o que deve estar em produção.

