import React from 'react'
import styled from 'styled-components'
import { TITLE_HEADER } from '../constants-components'
import { colors } from '../../assets/styles/default-style'
import { HEADER, MAIN_TITLE } from '../../utils/test-ids'

const Header = () => (
  <StyleHeader
    data-testid={HEADER}
  >
    <h1
      data-testid={MAIN_TITLE}
    >
      {TITLE_HEADER}
    </h1>
  </StyleHeader>
)

const StyleHeader = styled.header`
  display: flex;
  justify-content: center;
  margin: 10px 0;
  width: 100%;

  h1 {
    color: ${colors.white};
    font-size: 1.75rem;
    font-weight: 900;
    letter-spacing: 1px;
  }
`

export default Header