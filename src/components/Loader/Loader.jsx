import React from 'react'
import styled from 'styled-components'
import { bool } from 'prop-types'
import { colors } from '../../assets/styles/default-style'
import { LOADER } from '../../utils/test-ids'

const Loader = ({ isCenter }) => <LoaderStyle data-testid={LOADER} isCenter={isCenter} />

export default Loader

Loader.propTypes = {
  'isCenter': bool
}

Loader.defaultProps = {
  'isCenter': true
}

const LoaderStyle = styled.div`
  animation: spin 1s linear infinite;
  border: 5px solid ${colors.purple};
  border-left-color: ${colors.lighterpurple};
  border-radius: 50%;
  height: 50px;
  margin: ${({ isCenter }) => isCenter ? '0 auto' : '0'};
  width: 50px;

  @keyframes spin {
    to {
      transform: rotate(360deg);
    }
`