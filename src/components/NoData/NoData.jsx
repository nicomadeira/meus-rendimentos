import React from 'react'
import styled from 'styled-components'
import { NO_DATA_DEFAULT_CONTENT, NO_DATA_ERROR_CONTENT } from '../constants-components'
import { mediaQueries } from '../../assets/styles/default-style'
import { useInsumes } from '../../contexts/InsumesContext'
import { NO_DATA, NO_DATA_TITLE } from '../../utils/test-ids'

const NoData = () => {
  const { errorRequest } = useInsumes()

  return (
    <NoDataStyle
      data-testid={NO_DATA}
    >
      <h2
        data-testid={NO_DATA_TITLE}
      >
        {errorRequest ? NO_DATA_ERROR_CONTENT.title : NO_DATA_DEFAULT_CONTENT.title}

      </h2>
      <p>{errorRequest ? NO_DATA_ERROR_CONTENT.description : NO_DATA_DEFAULT_CONTENT.description}</p>
      {errorRequest && NO_DATA_ERROR_CONTENT.element}
    </NoDataStyle>
  )
}

export default NoData

const NoDataStyle = styled.div`
  background-color: rgba(0, 0, 0, .1);
  border-radius: 3px;
  margin: 0 auto;
  padding: 40px 50px;
  text-align: center;
  width: calc(100% - 80px);

  p {
    font-size: .875rem;
  }

  @media (min-width: ${mediaQueries.tablet.min}px) {
    padding: 50px 80px;
  }
`
