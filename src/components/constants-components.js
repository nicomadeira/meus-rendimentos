/* eslint-disable react/jsx-max-props-per-line */
/* eslint-disable react/react-in-jsx-scope */
import React from 'react'
import { isMobile } from '../utils/helpers'
import { NO_DATA_LINK } from '../utils/test-ids'

export const TITLE_HEADER = 'meus rendimentos'
export const INFO_FILTER = 'Você está vendo o período '
export const LABEL_FILTER = 'Clique ou aperte Enter + Seta para baixo, para escolher um período: '

export const NO_DATA_DEFAULT_CONTENT = {
  title: 'Nenhum dado sobre rendimentos foi encontrado no período selecionado.',
  description: 'Tente com outro período ou tente atualizar a página.',
}

export const NO_DATA_ERROR_CONTENT = {
  title: 'Ocorreu um erro ao acessar seus dados.',
  description: 'Tente novamente ou entre em contato conosco através do email: ',
  element: <a data-testid={NO_DATA_LINK} href="mailto:nicollemguimaraes@gmail.com">nicollemguimaraes@gmail.com</a>,
}

export const PERIOD_FILTER_DATA = [
  {
    value: 0,
    name: 'Desde o início',
    amoutOfDays: 0,
  },
  {
    value: 1,
    name: 'Último mês',
    amoutOfDays: 30,
  },
  {
    value: 2,
    name: '3 meses',
    amoutOfDays: 90,
  },
  {
    value: 3,
    name: '1 ano',
    amoutOfDays: 365,
  },
  {
    value: 4,
    name: '2 anos',
    amoutOfDays: 730,
  },
]

export const HELPER_INFO_GRAPH = `${isMobile() ? 'Clique' : 'Passe o mouse'} na área do gráfico para ver mais detalhes.`
