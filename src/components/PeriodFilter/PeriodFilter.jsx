import React, { useState, useRef, useEffect } from 'react'
import styled from 'styled-components'
import { colors, mediaQueries } from '../../assets/styles/default-style'
import { INFO_FILTER, LABEL_FILTER, PERIOD_FILTER_DATA } from '../constants-components'
import { ReactComponent as CalendarIcon } from '../../assets/images/calendar.svg'
import { ReactComponent as ArrowIcon } from '../../assets/images/down-arrow.svg'
import { useInsumes } from '../../contexts/InsumesContext'
import useKeyboard from '../../custom-hooks/useKeyboard'
import {
  LABEL_SELECT_FILTER,
  PERIOD_FILTER,
  SELECT_FILTER,
  SELECT_FILTER_OPTION,
  TEXT_INFO_FILTER,
} from '../../utils/test-ids'

const PeriodFilter = () => {
  const refContainer = useRef(null)
  const { filterInsumesByChosenPeriod, errorRequest } = useInsumes()
  const [selectedValue, setSelectedValue] = useState(0)
  const enterKeyPressed = useKeyboard('Enter')
  const tabKeyPressed = useKeyboard('Tab')

  const handlePeriodSelected = event => {
    const { value } = event.target
    const logicByFilterSelected = PERIOD_FILTER_DATA.find(filter => filter.value === Number(value))
    filterInsumesByChosenPeriod(logicByFilterSelected.amoutOfDays)
    setSelectedValue(value)
  }

  const handleSelectFocus = () => {
    refContainer.current.focus()
  }

  useEffect(() => {
    if (enterKeyPressed || tabKeyPressed) {
      handleSelectFocus()
    }
  }, [enterKeyPressed, tabKeyPressed])

  return (
    <PeriodFilterStyle
      data-testid={PERIOD_FILTER}
    >
      <>
        <div className="form-filter">
          <label
            data-testid={LABEL_SELECT_FILTER}
            htmlFor="period-filter"
          >
            {LABEL_FILTER}
          </label>
          <div className="row-form">
            <select
              className="select-filter"
              data-testid={SELECT_FILTER}
              disabled={errorRequest}
              id="period-filter"
              onChange={handlePeriodSelected}
              ref={refContainer}
              value={selectedValue}
            >
              {PERIOD_FILTER_DATA.map(filter => (
                <option
                  data-testid={SELECT_FILTER_OPTION}
                  key={filter.value + filter.name}
                  value={filter.value}
                >
                  {filter.name}
                </option>
              ))}
            </select>
            <ArrowIcon className="arrow-icon" />
          </div>
        </div>
        <div className="info-filter">
          <CalendarIcon className="calendar-icon" />
          <p data-testid={TEXT_INFO_FILTER}>
            {INFO_FILTER}
            <strong>{PERIOD_FILTER_DATA[selectedValue].name.toLowerCase()}</strong>
          </p>
        </div>
      </>
    </PeriodFilterStyle>
  )
}

export default PeriodFilter

const PeriodFilterStyle = styled.div`
  display: inline-flex;
  flex-direction: column;
  margin-bottom: 20px;
  width: calc(100% - 80px);

  .form-filter {
    align-items: center;
    display: flex;
    justify-content: flex-end;
    padding-bottom: 20px;
    text-align: right;

    .row-form {
      position: relative;
    }

    .select-filter {
      -webkit-appearance: none;
      background-color: rgba(255, 255, 255, .2);
      border: none;
      border-radius: 3px;
      color: #fff;
      cursor: pointer;
      font-family: 'PT Sans', sans-serif;
      font-size: 16px;
      padding: 5px 0;
      padding-left: 5px;
      padding-right: 30px;

      &:hover,
      &:focus {
        &:after {
          width: 100%;
        }
      }

      &:after {
        background-color: rgba(255, 255, 255, .2);
        border-radius: 3px;
        content: '';
        height: 100%;
        left: 0;
        position: absolute;
        top: 0;
        transition: all .3s ease-in-out;
        width: 0;
        z-index: -1;
      }
    }
  }

  .calendar-icon {
    height: 20px;
    margin-right: 10px;
    width: 20px;
  }
  
  .arrow-icon {
    height: 15px;
    position: absolute;
    right: 12px;
    top: 7px;
    width: 15px;
  }
  
  .info-filter {
    align-items: center;
    background-color: ${colors.purple};
    border-radius: 3px;
    display: flex;
    justify-content: center;
    padding: 15px 20px;

    p {
      color: ${colors.white};
      font-size: 1rem;
      margin: 0;
      padding-top: 2px;
    }
  }

  @media (max-width: ${mediaQueries.mobile.max}px) {
    .form-filter {
      flex-direction: column;
      text-align: center;

      .row-form {
        margin-top: 20px;
        width: 100%;
      }

      .select-filter {
        width: 100%;
      }

      .arrow-icon {
        right: 10px;
        top: 8px;
      }
    }

    .info-filter {
      justify-content: flex-start;
      padding: 5px 10px;

      p {
        max-width: 220px;
      }
    }
  
    .calendar-icon {
      height: 30px;
      width: 30px;
    }
  }

  @media (min-width: ${mediaQueries.tablet.max}px) {
    margin-bottom: 30px;

    .info-filter {
      font-size: 1.125rem;
    }

    .select-filter {
      margin-right: 5px;
      margin-left: 10px;
    }
  }
`
