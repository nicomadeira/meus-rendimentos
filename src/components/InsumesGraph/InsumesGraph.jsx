import React, { useLayoutEffect, useState } from 'react'
import styled from 'styled-components'
import {
  AreaChart,
  XAxis,
  YAxis,
  Area,
  Tooltip,
  CartesianGrid,
} from 'recharts'
import CustomTooltip from './CustomTooltip'
import { formatDateToDisplay, formatMoneyToThousandsToDisplay } from '../../utils/formatters'
import { colors, mediaQueries } from '../../assets/styles/default-style'
import { calculateWindowSizeToResizeWrapper, isMobile } from '../../utils/helpers'
import { HELPER_INFO_GRAPH } from '../constants-components'
import { useInsumes } from '../../contexts/InsumesContext'
import { INFO_GRAPH, INSUMES_GRAPH } from '../../utils/test-ids'

const InsumesGraph = () => {
  const { filteredInsumes } = useInsumes()
  const [graphSize, setGraphSize] = useState(300)

  useLayoutEffect(() => {
    let windowSize = isMobile() ? window.outerWidth : window.innerWidth
    let calculatedSize = calculateWindowSizeToResizeWrapper(windowSize)

    const resizeWindow = window.addEventListener('resize', () => {
      windowSize = window.outerWidth < 768 ? window.outerWidth : window.innerWidth
      calculatedSize = calculateWindowSizeToResizeWrapper(windowSize)
      setGraphSize(calculatedSize)
    })

    setGraphSize(calculatedSize)
    return () => window.removeEventListener('resize', resizeWindow)
  }, [])

  const renderCustomYAxisTick = value => formatMoneyToThousandsToDisplay(value)

  const renderCustomXAxisTick = value => formatDateToDisplay(value)

  return (
    <InsumesGraphStyle
      data-testid={INSUMES_GRAPH}
    >
      <div className="wrapper-graph">
        <AreaChart
          data={filteredInsumes}
          height={350}
          margin={{
            top: 20,
            right: 20,
            bottom: 0,
            left: 20,
          }}
          width={graphSize}
        >
          <defs>
            <linearGradient
              id="colorInsume"
              x1="0"
              y1="0"
              x2="0"
              y2="1"
            >
              <stop
                offset="5%"
                stopColor={colors.purple}
                stopOpacity={0.8}
              />
              <stop
                offset="95%"
                stopColor={colors.purple}
                stopOpacity={0.2}
              />
            </linearGradient>
          </defs>
          <XAxis
            interval="preserveStartEnd"
            dataKey="timestamp"
            tickFormatter={renderCustomXAxisTick}
            tickLine={false}
            tickMargin={5}
          />
          <YAxis
            tickFormatter={renderCustomYAxisTick}
            tickLine={false}
            tickMargin={5}
          />
          <Area
            type="monotone"
            dataKey="insume"
            stroke={colors.lighterpurple}
            fillOpacity={1}
            fill="url(#colorInsume)"
            strokeWidth={3}
          />
          <Tooltip content={<CustomTooltip />} />
          <CartesianGrid
            stroke="#fff"
            strokeDasharray="1"
            vertical={false}
          />
        </AreaChart>
      </div>
      <p
        data-testid={INFO_GRAPH}
      >
        <small>
          {HELPER_INFO_GRAPH}
        </small>
      </p>
    </InsumesGraphStyle>
  )
}

export default InsumesGraph

const InsumesGraphStyle = styled.div`
  p {
    text-align: center;
  }

  .wrapper-graph {
    .recharts-cartesian-axis-line {
      stroke: ${colors.white};
    }
  
    tspan {
      fill: ${colors.white};
    }
  
    .recharts-xAxis .recharts-layer {
      display: none;
    }
  
    .recharts-xAxis .recharts-layer:first-of-type,
    .recharts-xAxis .recharts-layer:last-of-type {
      display: block;
    }

    @media (max-width: ${mediaQueries.mobile.max}px) {
      margin-left: -20px;
    }

    @media (min-width: ${mediaQueries.tablet.min}px) {
      margin-left: -55px;
    }
  }

  @media (min-width: ${mediaQueries.mobile.max}px) {
    p {
      text-align: right;
    }
  }
`
