import React from 'react'
import {
  arrayOf, bool, number, object,
} from 'prop-types'
import styled from 'styled-components'
import { colors } from '../../assets/styles/default-style'
import { formatCurrency, formatDateToDisplay } from '../../utils/formatters'
import { CUSTOM_TOOLTIP_GRAPH, CUSTOM_TOOLTIP_GRAPH_LABEL } from '../../utils/test-ids'

const CustomTooltip = ({ active, payload, label }) => {
  if (active && payload && label) {
    return (
      <CustomTooltipStyle
        data-testid={CUSTOM_TOOLTIP_GRAPH}
      >
        <div className="wrapper-tooltip">
          <p
            className="tooltip-label"
            data-testid={CUSTOM_TOOLTIP_GRAPH_LABEL}
          >
            <span>{`Em ${formatDateToDisplay(label, true)}, `}</span>
            o total dos seus rendimentos foi:
            <strong>{formatCurrency(payload[0].value)}</strong>
          </p>
        </div>
      </CustomTooltipStyle>
    )
  }

  return null
}

export default CustomTooltip

CustomTooltip.defaultProps = {
  active: false,
  label: 0,
  payload: [],
}

CustomTooltip.propTypes = {
  active: bool,
  label: number,
  payload: arrayOf(object),
}

const CustomTooltipStyle = styled.div`
  background-color: ${colors.white};
  border-radius: 3px;
  padding: 10px 20px;

  .wrapper-tooltip {
    margin: 0 auto;
  }

  .tooltip-label {
    color: ${colors.darkblue};
    font-size: 13px;
    text-align: center;

    span,
    strong {
      display: block;
    }
    
    strong { 
      margin-top: 5px;
    }
  }
`
