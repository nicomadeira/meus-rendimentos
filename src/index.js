import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import { InsumesProvider } from './contexts/InsumesContext'

ReactDOM.render(
  <React.StrictMode>
    <InsumesProvider>
      <App />
    </InsumesProvider>
  </React.StrictMode>,
  document.getElementById('root'),
)
