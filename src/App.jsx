import React from 'react'
import styled from 'styled-components'
import GlobalStyle from './assets/styles/GlobalStyle'
import Home from './pages/Home/Home'
import Header from './components/Header/Header'
import { mediaQueries, wrapperSizes } from './assets/styles/default-style'

function App() {
  return (
    <>
      <GlobalStyle />
      <Header />
      <MainWrapper className="App">
        <Home />
      </MainWrapper>
    </>
  )
}

export default App

const MainWrapper = styled.main`
  align-items: center;
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin: 0 auto;
  max-width: calc(100vw - 30px);
  min-height: calc(100vh - 56px);
  width: 100%;

  @media (max-width: ${mediaQueries.mobile.max}px) {
    padding-bottom: 20px;
  }

  @media (min-width: ${mediaQueries.tablet.min}px) {
    max-width: ${wrapperSizes.tablet}px;
  }

  @media (min-width: ${mediaQueries.desktop.min}px) {
    max-width: ${wrapperSizes.desktop}px;
  }

  @media (min-width: ${mediaQueries.largedesktop.min}px) {
    max-width: ${wrapperSizes.largedesktop}px;
  }
`
