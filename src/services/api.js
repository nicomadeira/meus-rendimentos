const getInsumesData = async signal => {
  const request = await fetch(`${process.env.REACT_APP_API_URL}`, { signal })
  return request
}

export default getInsumesData
