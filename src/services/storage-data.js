const dataKey = process.env.REACT_APP_STORAGE_KEY

const setData = (data, key = dataKey) => {
  localStorage.setItem(key, data)
}

export default setData
