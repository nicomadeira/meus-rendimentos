import React, { useEffect } from 'react'
import styled from 'styled-components'
import InsumesGraph from '../../components/InsumesGraph/InsumesGraph'
import PeriodFilter from '../../components/PeriodFilter/PeriodFilter'
import Loader from '../../components/Loader/Loader'
import { useInsumes } from '../../contexts/InsumesContext'
import { HOME_PAGE } from '../../utils/test-ids'
import NoData from '../../components/NoData/NoData'

const Home = () => {
  const {
    errorRequest,
    fetchInsumesData,
    filteredInsumes,
    loaderRequest,
  } = useInsumes()

  useEffect(() => {
    let mounted = true
    const abortController = new AbortController()
    const { signal } = abortController
    async function fetchData() {
      await fetchInsumesData(signal)
    }

    if (mounted) {
      fetchData()
    }

    return () => {
      mounted = false
      abortController.abort()
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  if (errorRequest) {
    return <NoData />
  }

  return (
    <HomeStyle
      data-testid={HOME_PAGE}
    >
      {loaderRequest
        ? <Loader />
        : (
          <>
            <PeriodFilter />
            {filteredInsumes && !filteredInsumes.length ? <NoData /> : <InsumesGraph />}
          </>
        )}
    </HomeStyle>
  )
}

export default Home

const HomeStyle = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
`
