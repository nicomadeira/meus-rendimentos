import React, {
  createContext, useContext, useMemo, useState,
} from 'react'
import validateTimestamp from '../utils/validators'
import setData from '../services/storage-data'
import getInsumesData from '../services/api'

const InsumesContext = createContext()

const initialState = {
  errorRequest: false,
  listOfInsumes: [],
  loaderRequest: true,
  filteredInsumes: [],
}

const InsumesProvider = props => {
  const [state, setState] = useState(initialState)

  const value = useMemo(() => ({
    setState,
    state,
  }), [state])

  return (
    <InsumesContext.Provider
      value={value}
      {...props}
    />
  )
}

function useInsumes() {
  const context = useContext(InsumesContext)
  if (!context) {
    throw new Error('useInsumes deve ser usado dentro de InsumesProvider')
  }

  const { state, setState } = context

  const setListOfInsumes = listOfInsumes => {
    setState(prevState => ({
      ...prevState,
      listOfInsumes,
    }))
  }

  const setFilteredInsumes = filteredInsumes => {
    setState(prevState => ({
      ...prevState,
      filteredInsumes,
    }))
  }

  const setErrorRequest = errorRequest => {
    setState(prevState => ({
      ...prevState,
      errorRequest,
    }))
  }

  const setLoaderRequest = loaderRequest => {
    setState(prevState => ({
      ...prevState,
      loaderRequest,
    }))
  }

  const filterInsumesByChosenPeriod = differenceOfDays => {
    const { listOfInsumes } = state
    if (listOfInsumes.length) {
      if (!differenceOfDays) {
        setData(JSON.stringify(listOfInsumes))
        setFilteredInsumes(listOfInsumes)
        return
      }

      const filteredDates = listOfInsumes.filter(date => {
        const filterDate = new Date(date.timestamp).getTime()
        const lastDay = listOfInsumes[listOfInsumes.length - 1].timestamp

        if (!validateTimestamp(lastDay) || !validateTimestamp(filterDate)) {
          return date
        }

        const difference = lastDay - filterDate
        const diffBetweenDays = Math.round(difference / (1000 * 60 * 60 * 24))
        return diffBetweenDays < differenceOfDays
      })

      setData(JSON.stringify(filteredDates))
      setFilteredInsumes(filteredDates)
    }
  }

  const fetchInsumesData = async signal => {
    try {
      const insumesData = await getInsumesData(signal)
        .then(response => response.json())
        .then(result => result.map(userInsumes => {
          const [timestamp, insume] = userInsumes
          return {
            timestamp,
            insume,
          }
        }))

      if (!insumesData.length) {
        setErrorRequest(true)
      }

      setListOfInsumes(insumesData)
      setFilteredInsumes(insumesData)
    } catch (error) {
      setErrorRequest(true)
    } finally {
      setLoaderRequest(false)
    }
  }

  return {
    errorRequest: state.errorRequest,
    fetchInsumesData,
    filteredInsumes: state.filteredInsumes,
    filterInsumesByChosenPeriod,
    listOfInsumes: state.listOfInsumes,
    loaderRequest: state.loaderRequest,
    setFilteredInsumes,
    setListOfInsumes,
  }
}

export { InsumesProvider, useInsumes }
