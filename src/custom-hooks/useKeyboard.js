import React, { useState } from 'react'

const useKeyboard = selectedKey => {
  const [keyPressed, setKeyPressed] = useState(false)

  const wasKeyPressed = info => {
    if (info.key === selectedKey) {
      setKeyPressed(info.type === 'keydown')
    }
  }

  React.useEffect(() => {
    window.addEventListener('keydown', wasKeyPressed)
    window.addEventListener('keyup', wasKeyPressed)

    return () => {
      window.removeEventListener('keydown', wasKeyPressed)
      window.removeEventListener('keyup', wasKeyPressed)
    }
  })

  return keyPressed
}

export default useKeyboard
