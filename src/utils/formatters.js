import validateTimestamp from './validators'

export const formatMoneyToThousandsToDisplay = moneyValue => {
  if (!moneyValue || !parseInt(moneyValue, 10)) {
    return ''
  }

  const formattedAmountOfMoney = moneyValue > 1000 ? `${Math.round(moneyValue / 1000)}K` : moneyValue
  return `${formattedAmountOfMoney}`
}

export const formatCurrency = value => {
  const defaultValue = 0
  const validatedValue = (!value || !parseInt(value, 10)) ? defaultValue : value
  const splitValue = validatedValue.toFixed(2).split('.')
  splitValue[0] = `R$ ${splitValue[0].split(/(?=(?:...)*$)/).join('.')}`
  return splitValue.join(',')
}

export const formatDateToDisplay = (timestamp, inFull = false) => {
  const months = [
    'Janeiro',
    'Fevereiro',
    'Março',
    'Abril',
    'Maio',
    'Junho',
    'Julho',
    'Agosto',
    'Setembro',
    'Outubro',
    'Novembro',
    'Dezembro',
  ]

  const transformedMonths = months.map(month => ({
    abreviation: month.substring(0, 3).toUpperCase(),
    fullName: month,
  }))

  if (!validateTimestamp(timestamp)) {
    return ''
  }

  const date = new Date(timestamp)
  const options = { year: 'numeric', month: 'numeric', day: '2-digit' }
  const formattedTime = date.toLocaleDateString('pt-BR', options)
  const [day, month, year] = formattedTime.split('/')
  const formattedMonth = transformedMonths[month - 1]
  const dateFormattedToReturn = inFull
    ? `${day} de ${formattedMonth.fullName} de ${year}`
    : `${day} ${formattedMonth.abreviation.toUpperCase()} ${year}`
  return dateFormattedToReturn
}
