const validateTimestamp = timestamp => {
  const valid = (new Date(timestamp)).getTime() > 0
  return valid
}

export default validateTimestamp
