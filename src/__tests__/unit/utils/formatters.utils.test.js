/* eslint-disable no-console */
import {
  formatMoneyToThousandsToDisplay,
  formatCurrency,
  formatDateToDisplay,
} from '../../../utils/formatters'
import validateTimestamp from '../../../utils/validators'

jest.mock('../../../utils/validators')

describe('Utils > Formatters', () => {
  describe('FormatMoneyToThousandsToDisplay', () => {
    test('Passing a number as parameter should be successful', () => {
      const validValue = 2000
      const expectedReturn = '2K'
      expect(formatMoneyToThousandsToDisplay(validValue)).toBe(expectedReturn)
    })

    test('Passing 0 as parameter should return empty string', () => {
      const validValue = 0
      const expectedReturn = ''
      expect(formatMoneyToThousandsToDisplay(validValue)).toBe(expectedReturn)
    })

    test('Passing a invalid value should return empty string', () => {
      const invalidValue = 'Anything'
      const expectedReturn = ''
      expect(formatMoneyToThousandsToDisplay(invalidValue)).toBe(expectedReturn)
    })
  })

  describe('FormatCurrency', () => {
    test('Passing a number should return an BRL transformed currency', () => {
      const validValue = 2000
      const expectedReturn = 'R$ 2.000,00'
      const format = formatCurrency(validValue)
      expect(format).toBe(expectedReturn)
    })

    test('Passing a invalid value should return R$ 0,00', () => {
      const invalidValue = 'Anything'
      const expectedReturn = 'R$ 0,00'
      const format = formatCurrency(invalidValue)
      expect(format).toBe(expectedReturn)
    })

    test('Passing null should return R$ 0,00', () => {
      const invalidValue = null
      const expectedReturn = 'R$ 0,00'
      const format = formatCurrency(invalidValue)
      expect(format).toBe(expectedReturn)
    })
  })

  describe('FormatDateToDisplay', () => {
    test('Passing a timestamp should return a formated date', () => {
      const timestamp = '2020-06-14T22:35:43.611Z'
      const expectedReturn = '14 JUN 2020'

      validateTimestamp.mockImplementation(() => true)

      const format = formatDateToDisplay(timestamp)
      expect(format).toBe(expectedReturn)
    })

    test('Passing a timestamp with flag inFull should return a formated date in full', () => {
      const timestamp = '2020-06-14T22:35:43.611Z'
      const expectedReturn = '14 de Junho de 2020'

      validateTimestamp.mockImplementation(() => true)

      const format = formatDateToDisplay(timestamp, true)
      expect(format).toBe(expectedReturn)
    })

    test('Passing a string should return an empty string', () => {
      const invalidValue = 'Anything'
      const expectedReturn = ''

      validateTimestamp.mockImplementation(() => false)

      expect(formatDateToDisplay(invalidValue)).toBe(expectedReturn)
    })

    test('Passing null should return an empty string', () => {
      const invalidValue = null
      const expectedReturn = ''

      validateTimestamp.mockImplementation(() => false)

      expect(formatDateToDisplay(invalidValue)).toBe(expectedReturn)
    })
  })
})
