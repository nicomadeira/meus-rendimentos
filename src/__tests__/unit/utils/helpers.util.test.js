import { range, calculateWindowSizeToResizeWrapper } from '../../../utils/helpers'
import { wrapperSizes } from '../../../assets/styles/default-style'

describe('Utils > Helpers', () => {
  describe('Range', () => {
    test('Passing a number for the size should return an array range from 0 to the number passed', () => {
      const size = 3
      const expectedReturn = [0, 1, 2]
      expect(range(size)).toEqual(expect.arrayContaining(expectedReturn))
    })

    test('Passing a number for the size and a starter point should return an array range from the starter point to the number passed', () => {
      const size = 3
      const starterPoint = 2
      const expectedReturn = [2, 3, 4]
      expect(range(size, starterPoint)).toEqual(expect.arrayContaining(expectedReturn))
    })

    test('Passing a string for the size should return an array with a 0', () => {
      const size = ''
      const expectedReturn = [0]
      expect(range(size)).toEqual(expect.arrayContaining(expectedReturn))
    })

    test('Passing a string for the size and a string for the starter point should return an array with a 0 string', () => {
      const size = ''
      const starterPoint = ''
      const expectedReturn = ['0']
      expect(range(size, starterPoint)).toEqual(expect.arrayContaining(expectedReturn))
    })

    test('Passing null for the size and null for the starter point should return an array with a 0', () => {
      const size = null
      const starterPoint = null
      const expectedReturn = [0]
      expect(range(size, starterPoint)).toEqual(expect.arrayContaining(expectedReturn))
    })
  })

  describe('CalculateWindowSizeToResizeWrapper', () => {
    test('Passing a mobile window size of 320, should return the value of the window size less 30', () => {
      const windowSize = 320
      const expectedReturn = 290
      expect(calculateWindowSizeToResizeWrapper(windowSize)).toBe(expectedReturn)
    })

    test('Passing a tablet window size of 768, should return a default size for tablet', () => {
      const windowSize = 768
      const expectedReturn = wrapperSizes.tablet
      expect(calculateWindowSizeToResizeWrapper(windowSize)).toBe(expectedReturn)
    })

    test('Passing a desktop window size of 1025, should return a default size for desktop', () => {
      const windowSize = 1025
      const expectedReturn = wrapperSizes.desktop
      expect(calculateWindowSizeToResizeWrapper(windowSize)).toBe(expectedReturn)
    })

    test('Passing an invalid value should return a default tablet value', () => {
      const invalidString = ''
      const invalidValue = null
      const expectedReturn = wrapperSizes.tablet
      expect(calculateWindowSizeToResizeWrapper(invalidString)).toBe(expectedReturn)
      expect(calculateWindowSizeToResizeWrapper(invalidValue)).toBe(expectedReturn)
    })
  })
})
