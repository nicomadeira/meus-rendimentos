import validateTimestamp from '../../../utils/validators'

describe('Utils > Validators', () => {
  describe('ValidateTimestamp', () => {
    test('Passing a valid timestamp should return true', () => {
      const timestamp = 1565308800000
      expect(validateTimestamp(timestamp)).toBeTruthy()
    })

    test('Passing a invalid timestamp should return false', () => {
      const invalidValue = 0
      expect(validateTimestamp(invalidValue)).toBeFalsy()
    })
  })
})
