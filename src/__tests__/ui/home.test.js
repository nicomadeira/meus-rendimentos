import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import { render, screen } from '@testing-library/react'
import Home from '../../pages/Home/Home'
import { InsumesProvider } from '../../contexts/InsumesContext'
import {
  HOME_PAGE, LOADER, NO_DATA, INSUMES_GRAPH, PERIOD_FILTER,
} from '../../utils/test-ids'

const mock = [
  { timestamp: 1565308800000, insume: 24960 },
  { timestamp: 1565568000000, insume: 24960 },
  { timestamp: 1565654400000, insume: 24963.28 },
  { timestamp: 1565740800000, insume: 24966.55 },
  { timestamp: 1565827200000, insume: 24969.83 },
]

const initialState = {
  state: {
    errorRequest: false,
    listOfInsumes: [],
    loaderRequest: true,
    filteredInsumes: [],
  },
  setState: () => { },
}

const initialStateWithData = {
  state: {
    errorRequest: false,
    listOfInsumes: mock,
    loaderRequest: false,
    filteredInsumes: mock,
  },
  setState: () => { },
}

const initialStateWithoutData = {
  state: {
    errorRequest: false,
    listOfInsumes: [],
    loaderRequest: false,
    filteredInsumes: [],
  },
  setState: () => { },
}

describe('<Home />', () => {
  it('should render the component', () => {
    render(
      <InsumesProvider value={initialStateWithData}>
        <Home data-testid={HOME_PAGE} />
      </InsumesProvider>,
    )

    const homePage = screen.getByTestId(HOME_PAGE)
    expect(homePage).toBeInTheDocument()
  })

  it('should render with <Loader /> if loaderRequest is true', () => {
    render(
      <InsumesProvider value={initialState}>
        <Home data-testid={HOME_PAGE} />
      </InsumesProvider>,
    )

    const loaderComponent = screen.getByTestId(LOADER)
    expect(loaderComponent).toBeInTheDocument()
  })

  it('should render with <NoData /> if errorRequest is true', () => {
    render(
      <InsumesProvider value={{ ...initialState, state: { errorRequest: true } }}>
        <Home data-testid={HOME_PAGE} />
      </InsumesProvider>,
    )

    const noDataComponent = screen.getByTestId(NO_DATA)
    expect(noDataComponent).toBeInTheDocument()
  })

  it('should render with <NoData /> if insumes is empty', () => {
    render(
      <InsumesProvider value={initialStateWithoutData}>
        <Home data-testid={HOME_PAGE} />
      </InsumesProvider>,
    )

    const noDataComponent = screen.getByTestId(NO_DATA)
    expect(noDataComponent).toBeInTheDocument()
  })

  it('should render with <InsumesGraph /> and <PeriodFilter /> if has data of insumes', () => {
    render(
      <InsumesProvider value={initialStateWithData}>
        <Home data-testid={HOME_PAGE} />
      </InsumesProvider>,
    )

    const insumesGraphComponent = screen.getByTestId(INSUMES_GRAPH)
    expect(insumesGraphComponent).toBeInTheDocument()

    const periodFilterComponent = screen.getByTestId(PERIOD_FILTER)
    expect(periodFilterComponent).toBeInTheDocument()
  })
})
