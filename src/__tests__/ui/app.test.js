import React from 'react'
import ReactDOM from 'react-dom'
import App from '../../App'
import { InsumesProvider } from '../../contexts/InsumesContext'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(
    <InsumesProvider>
      <App />
    </InsumesProvider>, div,
  )
})
