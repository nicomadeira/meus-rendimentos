/* eslint-disable array-callback-return */
import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import { render, screen } from '@testing-library/react'
import { INFO_GRAPH, INSUMES_GRAPH } from '../../utils/test-ids'
import InsumesGraph from '../../components/InsumesGraph/InsumesGraph'
import { InsumesProvider } from '../../contexts/InsumesContext'
import { formatDateToDisplay } from '../../utils/formatters'

const mock = [
  { timestamp: 1565308800000, insume: 24960 },
  { timestamp: 1565568000000, insume: 24960 },
  { timestamp: 1565654400000, insume: 24963.28 },
  { timestamp: 1565740800000, insume: 24966.55 },
  { timestamp: 1565827200000, insume: 24969.83 },
]

const initialStateWithData = {
  state: {
    errorRequest: false,
    listOfInsumes: mock,
    loaderRequest: false,
    filteredInsumes: mock,
  },
  setState: () => { },
}

describe('<InsumesGraph />', () => {
  describe('<AreaChart />', () => {
    it('should render the component', () => {
      render(
        <InsumesProvider value={initialStateWithData}>
          <InsumesGraph data-testid={INSUMES_GRAPH} />
        </InsumesProvider>,
      )

      const insumesGraphComponent = screen.getByTestId(INSUMES_GRAPH)
      expect(insumesGraphComponent).toBeInTheDocument()

      const infoInsumesGraphInnerComponent = screen.getByTestId(INFO_GRAPH)
      expect(infoInsumesGraphInnerComponent).toBeInTheDocument()
    })

    it('should render graph with data', () => {
      render(
        <InsumesProvider value={initialStateWithData}>
          <InsumesGraph data-testid={INSUMES_GRAPH} />
        </InsumesProvider>,
      )

      mock.map(data => {
        const { timestamp } = data
        const formatTimestamp = formatDateToDisplay(timestamp)
        expect(screen.getByText(formatTimestamp)).toBeInTheDocument()
      })
    })
  })
})
