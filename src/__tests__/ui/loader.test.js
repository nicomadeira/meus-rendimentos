import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import { render, screen } from '@testing-library/react'
import { LOADER } from '../../utils/test-ids'
import Loader from '../../components/Loader/Loader'

describe('<Loader />', () => {
  it('should render the component', () => {
    render(<Loader data-testid={LOADER} />)

    const loaderComponent = screen.getByTestId(LOADER)
    expect(loaderComponent).toBeInTheDocument()
  })

  it('should have margin 0 in css when props isCenter is false', () => {
    render(<Loader
      data-testid={LOADER}
      isCenter={false}
    />)

    const loaderComponent = screen.getByTestId(LOADER)
    expect(loaderComponent).toHaveStyle('margin: 0;')
  })

  it('should have margin 0 auto in css when props isCenter is true', () => {
    render(<Loader
      data-testid={LOADER}
      isCenter
    />)

    const loaderComponent = screen.getByTestId(LOADER)
    expect(loaderComponent).toHaveStyle('margin: 0 auto;')
  })
})
