import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import { render, screen } from '@testing-library/react'
import { HEADER, MAIN_TITLE } from '../../utils/test-ids'
import Header from '../../components/Header/Header'

describe('<Header />', () => {
  it('should render the component', () => {
    render(<Header data-testid={HEADER} />)

    const headerComponent = screen.getByTestId(HEADER)
    expect(headerComponent).toBeInTheDocument()

    const mainTitle = screen.getByTestId(MAIN_TITLE)
    expect(mainTitle).toBeInTheDocument()
  })
})
