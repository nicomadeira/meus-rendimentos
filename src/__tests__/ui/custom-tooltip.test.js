import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import { render, screen } from '@testing-library/react'
import { CUSTOM_TOOLTIP_GRAPH, CUSTOM_TOOLTIP_GRAPH_LABEL } from '../../utils/test-ids'
import CustomTooltip from '../../components/InsumesGraph/CustomTooltip'

const inititalProps = {
  active: true,
  label: 1571961600000,
  payload: [{ value: 115288.13 }],
}

const timestamp = new Date(inititalProps.label).getDate()

const textTooltip = `Em ${timestamp} de Outubro de 2019, o total dos seus rendimentos foi:R$ 115.288,13`

describe('<Header />', () => {
  it('should render the component', () => {
    render(
      <CustomTooltip
        active={inititalProps.active}
        data-testid={CUSTOM_TOOLTIP_GRAPH}
        label={inititalProps.label}
        payload={inititalProps.payload}
      />,
    )

    const custoTooltipComponent = screen.getByTestId(CUSTOM_TOOLTIP_GRAPH)
    expect(custoTooltipComponent).toBeInTheDocument()

    const labelCustomTooltip = screen.getByTestId(CUSTOM_TOOLTIP_GRAPH_LABEL)
    expect(labelCustomTooltip.textContent).toBe(textTooltip)
  })

  it('should not render the component if prop active is false', () => {
    render(
      <CustomTooltip
        active={false}
        data-testid={CUSTOM_TOOLTIP_GRAPH}
        label={inititalProps.label}
        payload={inititalProps.payload}
      />,
    )

    const custoTooltipComponent = screen.queryByTestId(CUSTOM_TOOLTIP_GRAPH)
    expect(custoTooltipComponent).toBeNull()
  })

  it('should not render the component if prop label is null', () => {
    render(
      <CustomTooltip
        active={false}
        data-testid={CUSTOM_TOOLTIP_GRAPH}
        label={null}
        payload={inititalProps.payload}
      />,
    )

    const custoTooltipComponent = screen.queryByTestId(CUSTOM_TOOLTIP_GRAPH)
    expect(custoTooltipComponent).toBeNull()
  })
})
