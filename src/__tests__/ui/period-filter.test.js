import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import { render, fireEvent, screen } from '@testing-library/react'
import {
  LABEL_SELECT_FILTER,
  PERIOD_FILTER,
  SELECT_FILTER,
  SELECT_FILTER_OPTION,
  TEXT_INFO_FILTER,
} from '../../utils/test-ids'
import PeriodFilter from '../../components/PeriodFilter/PeriodFilter'
import { InsumesProvider } from '../../contexts/InsumesContext'
import { PERIOD_FILTER_DATA } from '../../components/constants-components'

describe('<PeriodFilter />', () => {
  it('should render the component', () => {
    render(
      <InsumesProvider>
        <PeriodFilter data-testid={PERIOD_FILTER} />
      </InsumesProvider>,
    )

    const periodFilterComponent = screen.getByTestId(PERIOD_FILTER)
    expect(periodFilterComponent).toBeInTheDocument()

    const selectFilterInnerComponent = screen.getByTestId(SELECT_FILTER)
    expect(selectFilterInnerComponent).toBeInTheDocument()

    const infoFilterInnerComponent = screen.getByTestId(TEXT_INFO_FILTER)
    expect(infoFilterInnerComponent).toBeInTheDocument()

    const labelFilterInnerComponent = screen.getByTestId(LABEL_SELECT_FILTER)
    expect(labelFilterInnerComponent).toBeInTheDocument()
  })

  it('info component should init with default value selected', () => {
    render(
      <InsumesProvider>
        <PeriodFilter data-testid={PERIOD_FILTER} />
      </InsumesProvider>,
    )

    const optionSelected = screen.getAllByTestId(SELECT_FILTER_OPTION)
    expect(optionSelected[0].selected).toBeTruthy()
  })

  it('should change the text while changing the value of the filter', () => {
    render(
      <InsumesProvider>
        <PeriodFilter data-testid={PERIOD_FILTER} />
      </InsumesProvider>,
    )

    const value = PERIOD_FILTER_DATA[2].name
    const selectFilter = screen.getByTestId(SELECT_FILTER)
    fireEvent.change(selectFilter, {
      target: { value },
    })

    expect(screen.getByText(value)).toBeInTheDocument()
  })
})
