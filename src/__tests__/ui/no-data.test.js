import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import { render, screen } from '@testing-library/react'
import { NO_DATA, NO_DATA_TITLE, NO_DATA_LINK } from '../../utils/test-ids'
import { InsumesProvider } from '../../contexts/InsumesContext'
import NoData from '../../components/NoData/NoData'
import { NO_DATA_DEFAULT_CONTENT, NO_DATA_ERROR_CONTENT } from '../../components/constants-components'

const initialStateWithoutData = {
  state: {
    errorRequest: false,
    listOfInsumes: [],
    loaderRequest: false,
    filteredInsumes: [],
  },
  setState: () => { },
}

const stateErrorRequest = { state: { ...initialStateWithoutData.state, errorRequest: true } }

describe('<NoData />', () => {
  it('should render the component with insumes empty', () => {
    render(
      <InsumesProvider value={initialStateWithoutData}>
        <NoData data-testid={NO_DATA} />
      </InsumesProvider>,
    )

    const noDataComponent = screen.getByTestId(NO_DATA)
    expect(noDataComponent).toBeInTheDocument()

    const titleNoDataComponent = screen.getByTestId(NO_DATA_TITLE)
    expect(titleNoDataComponent.textContent).toEqual(NO_DATA_DEFAULT_CONTENT.title)
  })

  it('should render the case of the component for error in request', () => {
    render(
      <InsumesProvider value={stateErrorRequest}>
        <NoData data-testid={NO_DATA} />
      </InsumesProvider>,
    )

    const noDataComponent = screen.getByTestId(NO_DATA)
    expect(noDataComponent).toBeInTheDocument()

    const titleNoDataComponent = screen.getByTestId(NO_DATA_TITLE)
    expect(titleNoDataComponent.textContent).toBe(NO_DATA_ERROR_CONTENT.title)

    const linkNoDataComponent = screen.getByTestId(NO_DATA_LINK)
    expect(linkNoDataComponent.getAttribute('href')).toBeDefined()
  })
})
