export const colors = {
  darkblue: '#274960',
  purple: '#8284b9',
  lighterpurple: '#9fa1e8',
  white: '#ffffff',
}

export const mediaQueries = {
  mobile: {
    min: 320,
    max: 767,
  },
  tablet: {
    min: 768,
    max: 1024,
  },
  desktop: {
    min: 1025,
    max: 1366,
  },
  largedesktop: {
    min: 1367,
    max: 2500,
  },
}

export const wrapperSizes = {
  tablet: 600,
  desktop: 780,
  largedesktop: 800,
}
